( () => {



    module.exports = ( config ) => {
        'use strict'



        config.modules.gulp.task('uglifyjs:production', ( production ) => {
            config.modules.gulp
            .src( `${config.foldersPath.dist}/angular-semantic-ui.js`)
            .pipe( config.modules.uglifyjs('angular-semantic-ui.min.js') )
            .pipe( config.modules.gulp.dest( config.foldersPath.dist ) )
        })
    }



} )()