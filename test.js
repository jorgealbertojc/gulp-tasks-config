( () => {



    module.exports = ( config ) => {
        'use strict'



        config.modules.gulp.task('test', ( done ) => {
            new config.modules.karmaServer({
                configFile: `${config.foldersPath.rootPath}karma.conf.js`,
                singleRun: false
            }, done).start()
        })
    }



} )()
