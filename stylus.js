

( () => {


    module.exports = ( config ) => {
        'use strict'


        config.modules.gulp.task('stylus', ['stylus:modules', 'stylus:app']);


        config.modules.gulp.task('stylus:modules', ( ) => {
            config.modules.gulp
            .src([
                `${config.foldersPath.source}/**/*.styl`,
                `!${config.foldersPath.source}/modules/**/_*.styl`,
                `!${config.foldersPath.source}/modules/**/mixins/**/*.styl`,
                `!${config.foldersPath.source}/modules/**/elements/**/*.styl`
            ])
            .pipe(config.modules.stylus())
            .pipe(config.modules.gulp.dest( `${config.foldersPath.build}` ))
        })


        config.modules.gulp.task('stylus:app', ( ) => {
            config.modules.gulp
            .src([
                `${config.foldersPath.source}/main-app/**/*.styl`
            ])
            .pipe(config.modules.stylus())
            .pipe( config.modules.concat('styles.css') )
            .pipe(config.modules.gulp.dest( `${config.foldersPath.build}/modules/commons` ))
          })


    }


} )()