( () => {



    module.exports = ( config ) => {
        'use strict'



        config.modules.gulp.task('typeScript:development', () =>
            config.modules.gulp
            .src( `${config.foldersPath.source}/modules/**/*.ts` )
            .pipe( config.modules.typeScript({
                noImplicitAny: true,
            }))
            .pipe(config.modules.gulp.dest( `${config.foldersPath.build}/modules` ))
        )



        config.modules.gulp.task('typeScript:production', () =>
            config.modules.gulp
            .src( `${config.foldersPath.source}/modules/**/*.ts` )
            .pipe( config.modules.typeScript({
                noImplicitAny: true,
                out: 'angular-semantic-ui.js'
            }))
            .pipe(config.modules.gulp.dest( config.foldersPath.dist ))
        )
    }



} )()