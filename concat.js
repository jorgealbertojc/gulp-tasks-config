( () => {



    module.exports = ( config ) => {
        'use strict'



        config.modules.gulp.task('concat', ( ) =>
            config.modules.gulp
            .src( `${config.foldersPath.source}/modules/**/*.js` )
            .pipe(config.modules.concat('angular-semantic-ui.js'))
            .pipe(config.modules.gulp.dest( config.foldersPath.dist ))
        )
    }



} )()
