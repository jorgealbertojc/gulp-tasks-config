

( () => {


    module.exports = ( config ) => {
        'use strict'


        config.modules.gulp.task('babel', ['babel:modules', 'babel:app']);


        config.modules.gulp.task('babel:modules', ( ) =>
            config.modules.gulp
            .src([
                `${config.foldersPath.source}/modules/**/*.js`,
            ])
            .pipe(
                config.modules.babel({
                    presets: ['es2015']
                })
            )
            .pipe( config.modules.gulp.dest( `${config.foldersPath.build}/modules/`  ))
        )


        config.modules.gulp.task('babel:app', ( ) =>
            config.modules.gulp
            .src([
                `${config.foldersPath.source}/main-app/**/*.js`
            ])
            .pipe(
                config.modules.babel({
                    presets: ['es2015']
                })
            )
            .pipe( config.modules.gulp.dest( `${config.foldersPath.build}/main-app/` ))
        )


    }


} )()