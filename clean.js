

( () => {


    module.exports = ( config ) => {
        'use strict'


        config.modules.gulp.task('clean:development', ( ) =>
            config.modules.gulp
            .src( config.foldersPath.build, {read: false})
            .pipe( config.modules.clean() )
        )


        config.modules.gulp.task('clean:production', ( ) =>
            config.modules.gulp
            .src( config.foldersPath.dist, {read: false})
            .pipe( config.modules.clean() )
        )
    }


} )()