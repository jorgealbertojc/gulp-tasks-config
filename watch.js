

( () => {


    module.exports = ( config ) => {
        'use strict'


        config.modules.gulp.task('watch', ( ) => {
            //config.modules.gulp.watch( `${config.foldersPath.source}/modules/**/*.ts`, ['typeScript:development'])
            // gulp.watch( config.modules.foldersPath.source + 'main.styl', ['stylus'])

            config.modules.gulp.watch([
                    `${config.foldersPath.source}/modules/**/*.js`
                ],
                [ 'babel:modules' ]
            )


            config.modules.gulp.watch( [
                    `${config.foldersPath.source}/main-app/**/*.js`
                ],
                [ 'babel:app' ]
            )


            config.modules.gulp.watch( `${config.foldersPath.source}/modules/**/*.styl`, ['stylus'])


            config.modules.gulp.watch( `${config.foldersPath.source}/modules/**/*.jade`, ['jade'])


        })
    }



} )()

