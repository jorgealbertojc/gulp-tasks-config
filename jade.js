( () => {



    module.exports = ( config ) => {
        'use strict'


        config.modules.gulp.task('jade', ( ) =>
            config.modules.gulp
            .src([
                `${config.foldersPath.source}/**/*.jade`,
                `!${config.foldersPath.source}/modules/**/mixins/**`,
                `!${config.foldersPath.source}/modules/**/layouts/**`,
                `!${config.foldersPath.source}/modules/commons/**`
            ])
            .pipe(config.modules.jade({
                pretty: true
            }))
            .pipe(config.modules.gulp.dest( `${config.foldersPath.build}` ))
        )
    }



} )()
