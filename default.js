( () => {



    module.exports = ( config ) => {
        'use strict'



        config.modules.gulp.task('default', config.modules.sync.sync(
            [
                // Sync
                'clean:development',
                [
                    // Async
                    'babel',
                    'jade',
                    'stylus',
                    'copy'
                ],
                'server',
                'watch'
            ]
        ))

        config.modules.gulp.task('production', config.modules.sync.sync(
            [
                'clean:production',
                //'typeScript:production',
                'concat',
                'uglifyjs:production',
            ]
        ))
    }



} )()
